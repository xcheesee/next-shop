module.exports = function (api) {
  api.cache(false);
  return {
    plugins: ['macros'],
    presets: ['@babel/preset-react', '@babel/preset-env', 'next/babel']
  }
}