import { useRouter } from 'next/router'

export default function Id () {
  
  const router = useRouter()
  const data = router.query
  console.log(data)
  return (
    <div className='grid grid-cols-[33%_1fr] p-20'>
     <div className=''>{data.id}</div>
     <div className='grid grid-cols-[1fr_1fr]'>
      <div>name</div>
      <div>price</div>
      <div className=' col-span-2 justify-self-center'>description</div>
     </div>
    </div>
  )
}