import ProductItem from "../../../components/ProductItem"
import React, {useState} from "react"
import ProductsHeader from "../../../components/ProductsHeader"
import ProductsNav from "../../../components/productsNav"
import CheckoutBtn from "../../../components/CheckoutBtn"
import cart from "../../../components/cartMethods"
import { CircularProgress } from '@mui/material';
import { Grid } from '@mui/material'


export default function Section ({prod}) {

  return (
    <div>
      <ProductsNav />
      {/* <div className="grid grid-cols-[min-content_min-content_1fr_max-content_max-content_min-content_max-content] gap-14 px-10 py-5"> */}
        <Grid Grid container spacing={5} justifyContent={'flex-end'}>
          <ProductsHeader />
          {prod.map((product, i) => <ProductItem product={product} key={i}/>)}
          <CheckoutBtn />
        </Grid>
      {/* </div> */}
    </div>
  )
}

export const getServerSideProps = async (context) => {
  try {
    const prodData = await fetch(`http://localhost:3000/${context.params.section}`)
    const prod = await prodData.json()
    if(prodData.status === 200) {
      return {
        props: {
          prod
        }
      }
    } else {
      throw new Error('ih')
    }
  } catch (err) {
    console.log(err)
    return {
      notFound: true
  }}
}