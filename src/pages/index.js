import {Splide, SplideSlide} from '@splidejs/react-splide'
import '@splidejs/react-splide/css'
import ProductMini from '../components/ProductMini'
import cart from '../components/cartMethods'
import { Box, Typography } from '@mui/material'


export default function Home(props) {

  return (
    <Box className="self-center flex flex-col gap-10 items-center">
      <Typography variant='h1' /* sx={{alignSelf: 'center'}} */>Namespace</Typography>

      <Box /* className='self-center' */>
        <Splide aria-label="My Favorite Images" options={{
          type: 'loop',
          speed: 1000,
          autoplay: true,
          width: 900,
          height: 'auto',
        }}>
          <SplideSlide>
            <img src="https://source.unsplash.com/1920x1080/?beach" alt="Image 1"/>
          </SplideSlide>
          <SplideSlide>
            <img src="https://source.unsplash.com/1920x1080/?dog" alt="Image 2"/>
          </SplideSlide>
          <SplideSlide>
            <img src="https://source.unsplash.com/1920x1080/?cat" alt="Image 2"/>
          </SplideSlide>
          <SplideSlide>
            <img src="https://source.unsplash.com/1920x1080/?blog" alt="Image 2"/>
          </SplideSlide>
        </Splide>
      </Box>
      

      <Box className='grid gap-10 grid-cols-1 p-10 lg:grid-cols-3'>
        <ProductMini description={'ola'} price={'R$ 5,99'}/>
        <ProductMini description={'marilene'} price={'R$ 29,99'}/>
        {/* <ProductMini description={'noite'} price={'R$ 15,99'}/>
        <ProductMini description={'tainha'} price={'R$ 24,99'}/>
        <ProductMini description={'vinho'} price={'R$ 0,99'}/>
        <ProductMini description={'e muita'} price={'R$ 99,99'}/>
        <ProductMini description={'alegria'} price={'R$ 222222,99'}/> */}
      </Box>
    </Box>
  )
}
