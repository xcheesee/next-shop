import React from 'react'
import Header from './Header'
import Footer from './Footer'

const Layout = ({children}) => {
  return (
    <div className='grid grid-cols-1 grid-rows-[min-content_1fr_min-content] w-full'>
      <Header />
      {children}
      <Footer />
    </div>
  )
}

export default Layout