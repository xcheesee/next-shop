import React from 'react'

const Footer = () => {
  return (
    <footer className='p-5 bg-slate-800 text-slate-50 relative bottom-0 text-center'>Made by XX</footer>
  )
}

export default Footer