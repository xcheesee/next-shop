import { Grid } from '@mui/material'

export default function ProductsHeader () {
  const headerClass = 'font-bold justify-self-center'
  return (
    <>
      <Grid item xs={1} className={`${headerClass}`}>Id</Grid>
      <Grid item xs={1} className={`${headerClass}`}>Name</Grid>
      <Grid item xs={2} className={`${headerClass}`}>Description</Grid>
      <Grid item xs={2} className={`${headerClass}`}>Price</Grid>
      <Grid item xs={2} className={`${headerClass}`}>Stock</Grid>
      <Grid item xs={2} className={`${headerClass}`}>Quantity</Grid>
      <Grid item xs={2} className={`${headerClass}`}>Buy</Grid>
    </>
  )
}