import Link from "next/link"

export default function ProductsNav () {
  return (
    <ul className="flex gap-5 shadow-lg justify-around">
      <li><Link href='/products/shirts'>Shirts</Link></li>
      <li>Pants</li>
      <li><Link href='/products/shoes'>Shoes</Link></li>
      <li>Weights</li>
      <li>Lorem</li>
      <li>Ipson</li>
      <li>Amet</li>
      <li>Null</li>
      <li>None</li>
    </ul>
  )
}