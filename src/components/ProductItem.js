import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faCartArrowDown} from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { Grid } from '@mui/material'

export default function ProductItem ({product}) {

  const router = useRouter()
  const toProductPage = {pathname: `${router.asPath + '/q'}`, query: product}
  const currencyFormat = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
  })
  const {id, name, description, price, inStock} = product

  return (
    <>
        <Grid item xs={1}>
          <Link href={toProductPage}>{id}</Link>
        </Grid>{/*imagem*/}
        <Grid item xs={1}>
          <Link href={toProductPage}>{name}</Link>
        </Grid>
        <Grid item xs={2}>{description}</Grid>
        <Grid item xs={2}>{currencyFormat.format(price)}</Grid>
        { inStock ? <Grid item xs={2} className="text-green-700">Available</Grid>
          : <Grid item xs={2} className="text-red-700">Not in stock</Grid>
        }
        <Grid item xs={2}>
        <input type='number' className="border-2" defaultValue={0}/>

        </Grid>
        <Grid item xs={2}>

        <div>Add to Cart &nbsp; <FontAwesomeIcon icon={faCartArrowDown} /></div>
        </Grid>
    </>
  )
}