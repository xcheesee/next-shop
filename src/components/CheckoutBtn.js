import Link from 'next/link'
import { Button, Grid } from '@mui/material'

export default function Checkout () {
  return (
    // <div className="mr-5 col-start-7 border-4 p-2 rounded-xl bg-green-500 border-green-500 text-white font-semibold">
      <Grid item xs={1} >
        <Button variant="outlined" className='flex-end'>
          <Link href='/'>Checkout Items</Link>
        </Button>
      </Grid>
    // </div>
  )
}