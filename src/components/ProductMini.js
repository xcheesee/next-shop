import React from "react";
import { Card, CardContent, CardMedia, Typography, Paper, Box } from '@mui/material'

export default function ProductMini ({image, description, price}) {

  return (
    <>
      <Paper elevation={10} sx={{minWidth: 300, maxWidth: 400}}>
        <Box
          component='img'
          alt="image"
          src="https://source.unsplash.com/1920x1080/?beach"
        />
        <h1 className="text-3xl">Image</h1>
        <p>{description}</p>
        <p>{price}</p>
      </Paper>
      <Card elevation={10} sx={{borderRadius: 3, minWidth: 300, maxWidth: 400}}>
        <CardMedia
          component='img' 
          image="https://source.unsplash.com/1920x1080/?beach"
        />
        <CardContent>
          <Typography variant="h4" color='-moz-initial'>
            {description}
          </Typography>
          <Typography variant="subtitle1">
            {price}
          </Typography>
        </CardContent>
      </Card>
    </>
    
  );
}