import React from 'react'
import Link from 'next/link'

const Header = () => {
  return (
    <div className='px-10 py-5 flex relative bg-neutral-500 text-slate-50'>
      {/* <Image src="" alt="Shop Logo" /> */}
      <p>Market</p>
      <nav className='absolute left-1/2 -translate-x-1/2 flex gap-5'>
        <Link href='/'>Home</Link>
        <Link href='/products'>Products</Link>
        <Link href='/about'>About Us</Link>
      </nav>
    </div>
  )
}

export default Header